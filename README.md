# Servidor Web Estático

Servidor Web Estático

# Para rodar o projeto
0. Instalar no ambiente de desenvolvimento o Node.js (https://nodejs.org).
1. Fazer o checkout do projeto utilizando: git clone https://gitlab.com/gilbriatore/2021/web/srv-web.git
2. Rodar, na pasta do servidor, o comando: npm install
3. Executar, na pasta do servidor, o comando: npm start
4. O servidor será iniciado em `http://localhost:3000/`. 
